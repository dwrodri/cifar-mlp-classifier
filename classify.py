import itertools
import sys
from typing import Any, Iterator, List, Sequence, Tuple, Type, Union

import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
import torchvision
from numpy.core.fromnumeric import squeeze
from PIL import Image
from torch import nn
from torch.nn.modules.rnn import LSTM
from torchvision import transforms
from torchvision.transforms.transforms import Resize
from tqdm import tqdm

BATCH_SIZE = 128
CIFAR10_IMG_SIZE = 3072


# class labels are one hot vectors, here is the mapping
classes = (
    "plane",
    "car",
    "bird",
    "cat",
    "deer",
    "dog",
    "frog",
    "horse",
    "ship",
    "truck",
)


# images are RBGA uint8 need to be float
transform = transforms.Compose(
    [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),]
)


def activation_func(activation) -> nn.Module:
    return nn.ModuleDict(
        {
            "relu": nn.ReLU(inplace=True),
            "leaky_relu": nn.LeakyReLU(negative_slope=0.01, inplace=True),
            "selu": nn.SELU(inplace=True),
            "none": nn.Identity(),
        }
    )[activation]


def sliding_window(seq: List[Any], window_size: int = 2) -> List[List[Any]]:
    """
    Return a sliding view over the data.

    For example, if you have [1,2,3,4,5], it converts to [[1,2],[2,3],[3,4],[4,5]]
    """
    return [seq[i - window_size + 1 : i + 1] for i in range(window_size - 1, len(seq))]


def generate_possible_stacks(
    config: Sequence[int], possible_values: List[int]
) -> Iterator[Tuple[int]]:
    yield from itertools.product(
        *[possible_values[:subset_end] for subset_end in config]
    )


class Auto2DConv(nn.Conv2d):
    """
    A depthwise-separable 2D convolution with automatic padding
    """

    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: Union[int, Tuple[int, int]],
        stride: Union[int, Tuple[int, int]] = (1, 1),
        dilation: Union[int, Tuple[int, int]] = (1, 1),
        bias: bool = False,
        padding_mode: str = "zeros",
    ):
        super().__init__(
            in_channels,
            out_channels,
            kernel_size,
            stride,
            (0, 0),
            dilation,
            in_channels if out_channels % in_channels == 0 else 1,
            # 1,
            bias,
            padding_mode=padding_mode,
        )
        self.padding = (self.kernel_size[0] // 2, self.kernel_size[1] // 2)


def BNConv(
    in_channels: int,
    out_channels: int,
    kernel_size: Union[int, Tuple[int, int]],
    stride: Union[int, Tuple[int, int]] = (1, 1),
    dilation: Union[int, Tuple[int, int]] = (1, 1),
    bias: bool = True,
    padding_mode: str = "zeros",
) -> nn.Sequential:
    return nn.Sequential(
        Auto2DConv(
            in_channels,
            out_channels,
            kernel_size,
            stride,
            dilation,
            bias,
            padding_mode,
        ),
        nn.BatchNorm2d(out_channels),
        nn.SiLU(inplace=True),
    )


class ShortCutModule(nn.Module):
    """
    Base class for shortcut inference
    """

    def __init__(self, block: nn.Module, shortcut: nn.Module, activation: str = "relu"):
        super().__init__()
        self._block, self._shortcut = block, shortcut
        self._activation = activation_func(activation)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self._activation(self._block(x) + self._shortcut(x))


class ResidualBlock(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: Union[int, Tuple[int, int]],
        stride: Union[int, Tuple[int, int]] = (1, 1),
        dilation: Union[int, Tuple[int, int]] = (1, 1),
        feature_scale: int = 1,
        activation: str = "relu",
    ):
        super().__init__()
        self._activation = activation_func(activation)
        self._block = nn.Sequential(
            BNConv(in_channels, out_channels, kernel_size, stride, dilation, bias=True),
            self._activation,
            BNConv(out_channels, out_channels * feature_scale, kernel_size, bias=True),
        )
        self._shortcut = (
            nn.Sequential(
                BNConv(
                    in_channels,
                    out_channels * feature_scale,
                    kernel_size=1,
                    stride=stride,
                    dilation=dilation,
                    bias=True,
                ),
                self._activation,
            )
            if in_channels != (out_channels * feature_scale)
            else nn.Identity()
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self._activation(self._block(x) + self._shortcut(x))


class ResidualBottleneck(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: Union[int, Tuple[int, int]],
        stride: Union[int, Tuple[int, int]] = (1, 1),
        dilation: Union[int, Tuple[int, int]] = (1, 1),
        feature_scale: int = 4,
        activation: str = "relu",
    ):
        super().__init__()
        self._activation = activation_func(activation)
        self._block = nn.Sequential(
            BNConv(in_channels, out_channels, 1, bias=True),
            self._activation,
            BNConv(
                out_channels, out_channels, kernel_size, stride, dilation, bias=True
            ),
            self._activation,
            BNConv(out_channels, out_channels * feature_scale, 1, bias=True),
        )
        self._shortcut = (
            nn.Sequential(
                BNConv(
                    in_channels,
                    out_channels * feature_scale,
                    kernel_size=1,
                    stride=stride,
                    dilation=dilation,
                    bias=True,
                ),
                self._activation,
            )
            if in_channels != out_channels * feature_scale
            else nn.Identity()
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self._activation(self._block(x) + self._shortcut(x))


class BetterConvNet(nn.Module):
    def __init__(self, layers: List[int]):
        super().__init__()
        self._required = nn.Sequential(
            Auto2DConv(in_channels=3, out_channels=32, kernel_size=5),
            nn.BatchNorm2d(32),
            nn.ReLU(inplace=True),
        )
        self._layers = nn.Sequential(
            *[
                ResidualBlock(in_ch, out_ch, kernel_size=3)
                for in_ch, out_ch in sliding_window([32] + layers)
            ]
        )
        self._out = nn.Linear(32 * 32 * 64, 10)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self._required(x)
        x = self._layers(x)
        return self._out(x.view(-1, 32 * 32 * 64))


class ConvStack(nn.Module):
    def __init__(
        self,
        channel_size_list: List[int],
        kernel_size_list: List[int],
        bottleneck_layer: Type[nn.Module] = nn.Identity,
    ):
        """
        Search-friendly way for generating Conv stacks
        """
        super().__init__()
        if len(channel_size_list) - 1 != len(kernel_size_list):
            raise RuntimeError("channels must be one longer than kernels")
        layers = []
        for kernel_size, in_chan, out_chan in map(
            lambda x: (x[0], *x[1]),
            zip(kernel_size_list, sliding_window(channel_size_list)),
        ):
            layers.append(BNConv(in_chan, out_chan, kernel_size))
            # layers.append(nn.ReLU(inplace=True))
        self._layers = nn.Sequential(*layers)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self._layers(x)


class CIFAR10ConvNet(nn.Module):
    def __init__(self, chans, kerns):
        super().__init__()
        self._last_dim = 32 * 32 * chans[-1]
        self._c1 = BNConv(in_channels=3, out_channels=32, kernel_size=5)
        self._convs = ConvStack(chans, kerns)
        self._fc1 = nn.Linear(self._last_dim, 256)
        self._out = nn.Linear(256, 10)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = F.silu(self._c1(x))
        x = self._convs(x)
        # print(x.shape)
        x = x.view(-1, self._last_dim)
        x = F.silu(self._fc1(x))
        return self._out(x)


def train(chans=[32, 64, 64, 128, 92, 64, 64], device=torch.device("cuda:0")):
    trainset = torchvision.datasets.CIFAR10(
        root="./data", train=True, download=True, transform=transform
    )
    trainloader = torch.utils.data.DataLoader(
        trainset, batch_size=BATCH_SIZE, shuffle=True, num_workers=2, pin_memory=True
    )
    testset = torchvision.datasets.CIFAR10(
        root="./data", train=False, download=True, transform=transform
    )
    testloader = torch.utils.data.DataLoader(
        testset, batch_size=BATCH_SIZE, shuffle=False, num_workers=2, pin_memory=True
    )
    kerns = [3] * (len(chans) - 1)
    for i in range(1, len(kerns), 2):
        kerns[i] = 1
    net = CIFAR10ConvNet(chans, kerns)  # type: ignore
    print(net)
    net.to(device)
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(net.parameters(), lr=0.004, weight_decay=0.0000)
    # torch.autograd.set_detect_anomaly(True)
    test_acc: float = 0.0
    for epoch in range(20):
        net.train()
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            inputs, labels = data[0].to(device), data[1].to(device)
            optimizer.zero_grad(set_to_none=True)
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
        correct = 0
        total = 0
        if epoch % 2 == 0:
            with torch.no_grad():
                net.eval()
                for data in testloader:
                    images, labels = data[0].to(device), data[1].to(device)
                    outputs = net(images)
                    _, predicted = torch.max(outputs.data, 1)
                    total += labels.size(0)
                    correct += (predicted == labels).sum().item()
            test_acc = 100 * correct / total
            print(
                f"{epoch+1} loss: {running_loss / (i+1):.3f}\ttest accuracy: {test_acc}%"
            )
        # if test_acc >= 40.00:
        #     print("Stopping early as we have met the 40% test accuracy goal")
        #     break

    if test_acc > 75.0:
        torch.save(
            {
                "epoch": epoch,
                "model_state_dict": net.state_dict(),
                "optimizer_state_dict": optimizer.state_dict(),
                "loss": running_loss,
            },
            "model/model.pt",
        )
        print("done")


def test(img_path: str) -> str:
    # Resize required for arbitrary image support
    img = Image.open(img_path).resize((32, 32), Image.BILINEAR)
    input_tensor = transform(img).unsqueeze(0)
    net = Net()
    net.load_state_dict(torch.load("model/model.pt")["model_state_dict"])
    # eval() sets dropout layers to evlauation mode
    net.eval()
    _, predicted = torch.max(net(input_tensor), 1)
    return classes[predicted.squeeze()]


if __name__ == "__main__":
    if len(sys.argv) == 3 and sys.argv[1] == "test":
        print(f"prediction result: {test(sys.argv[2])}")
    else:
        # chans = [32, 32, 64, 64, 128, 128, 64, 64, 64, 32]
        # kerns = [3, 1, 3, 1, 3, 1, 3, 1, 3]
        # print(
        #     sum(
        #         [
        #             1
        #             for _ in generate_possible_stacks(
        #                 [1, 2, 3, 3, 4, 3, 2, 1], [32, 48, 64, 96, 128]
        #             )
        #         ]
        #     )
        # )
        train()
