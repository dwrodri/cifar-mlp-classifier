import torch
from torch import nn

if __name__ == "__main__":
    source = torch.ones((1, 3, 32, 32))
    conv = nn.Conv2d(in_channels=3, out_channels=32, kernel_size=5)
    print(conv(source).shape)